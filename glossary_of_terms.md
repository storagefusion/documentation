#Glossary of Report Fields (Domain)

`Adapter ID` (Array Port) – The identifier for the adapter card that a port belongs to.

`Allocated Capacity` (Array) - This is the total amount of storage allocated to hosts (it is the sum of Configured Sizes of all LUNs assigned to hosts).

`Array - Inactive` - These arrays have no storage presented to hosts or other arrays at all so are not in use.

`Array – Active` – These arrays do have storage presented to hosts or other arrays.

`Array Name` – The name associated with the storage array.

`Chassis` (Disk) – The primary location for a disk drive – often the rack or cabinet.

`CLI Version` (Array) – The software level of the Vendor command line management interface used for the collection.

`Collection Date` (Array) – The date and time that the data being displayed was collected (not uploaded).

`Configured Capacity` (Pool) – The amount of usable storage in a pool – the pool size (Includes allocated and unallocated storage pool space).

`Configured Size` (LUN) – The storage allocated by the array to the LUN.

`Data Container` (Reporting) – This is the place where all nodes that belong to a reporting entity are stored (e.g. Company).

`Disks` – The base storage components that make up the storage in an array (e.g. HDD).

`Dynamic Tiering` (Pool) – This is a feature of an array that allows the array to perform workload balancing across its available disk technologies.

`Entity Groups` (Array, Host) – This is a dynamic grouping of nodes (Hosts and Arrays) within the product to match business function or other logical separation of resources.

`EOSL` (Array) – The End of Service Life date is the date when manufacturers cease support for an array based on published vendor announcements.

`Family` (Array) – This is the array vendor product group.

`FC` – Fibre Channel.

`Firmware Version` – The version of firmware installed on the storage array.

`Free Capacity` (Array, Pool) – The amount of storage within an array or pool that is unassigned and available to be assigned to hosts.

`Free Raw Capacity` (Array) – The total size of drives that have been installed in the system that have not been configured for use or spares.

`Formatted Capacity` (Disk, Array) – The size or total size of physical drives after formatting.

`GiB`– Gibibyte (1GiB = 230 Byte = 1,073,741,824 Byte).

`Hosts` (Array, Host) – A host is a consumer of storage attached to an array.

`Host Connections` (Array) – This is a unique and discrete path between a host and the attached array.

`Installed Memory` (Array) – The amount of cache RAM installed in the array.

`IP Address` (Array, Host) – This is the ‘Internet Protocol address of an Array or Host connection.

`KiB` – Kibibyte (1 KiB = 210 Bytes = 1024 Bytes)

`Local Copy Capacity` (Array) – The total amount of storage allocated to holding local copies of data in an array.

`LUN` – Logical unit number – used to define a logical unit of storage within a storage network.

`LUN ID` (LUN) – The identifier of a LUN at array or host level.

`LUN Name` (LUN) – This is the optional name of a LUN.

`LUN Type` (LUN) – This is the purpose of the LUN (e.g. Primary)

`Machine Information` (Array) – Additional information about an array – supplemental to the other quantitative information displayed.

`Master Pool` (Pool) – In the context of nested pools this is the primary pool that owns the storage resources within the array.

`MiB` – Mebibyte (1 MiB = 220 Byte = 1,048,576 Byte).

`Model` (Array, Disk) – This is the product name assigned by the manufacturer.

`Pool` (Arrays) – A collection of storage setup within an array with specific
features or capabilities in common.

`Pool Name` (Pool) – The name of the storage pool.

`Pool Purpose` (Pool) – The use that the pool is put to (e.g. Thin Provisioning).

`Port` (Array) – An array connection point to the storage network.

`Port Number` (Port) – A unique number within the array given to a specific port.

`Power Idle` (Disk) – The amount of power consumed by a drive at rest according to the manufacturer.

`Power Peak` (Disks) – The amount of power consumed by a drive during a typical operation cycle according to the manufacturer.

`Primary Capacity` (Array, Host) – This is the total amount of storage allocated to primary devices at either an array or host level.

`Provider` (LUN) – The source of the LUN (e.g. pool, raid group or array).

`RAID Type` (Pool, LUN) – This is the level of disk failure protection within the LUN or pool.

`Raw Capacity` (Disk, Array) – This is the total unformatted size of an individual disk or all of the disks in an array according to the disk manufacturer.

`Replication Capacity` (Array) – The total amount of storage allocated to holding copies of data from other arrays.

`Reserved Capacity` (Array, Pool) – The total free space within an individual pool, or all of the pools within an array.

`SAS` (Disk) – Serial Attached SCSI disks.

`SATA` (Disk) – Serial AT Attached disks.

`Shelf` (Disk) – This is the intermediate location of a disk within the chassis of an array.

`Serial Number` (Array, Disk) – This is the unique device number assigned to an array or disk by its manufacturer.

`Slot` (Disk) – The specific location within the shelf of an array.

`Speed` (GiB/s) (Port) – The speed of a ports transmission/receive rate.

`SSD` (Disk) – Solid State Drive disks.

`Sub Pool` (Pool) – In a nested pool environment a subordinate pool taking its capacity from a master pool (e.g. IBM SVC child pool).

`Subscription Ratio` (Pool) – The subscribed capacity in a pool expressed as a percentage of the configured capacity of the pool.

`Subscribed Capacity` (Pool) – The total amount of storage assigned as seen from the host perspective.

`Subscribed Size` (LUN) – The apparent size of a LUN seen from the hosts perspective.

`Thin Provisioning` (Pool, LUN) – The feature that allows administrators to assign capacity to hosts as requested but that allow the array to only allocate storage as its written to by the host.

`TiB` – Tibibyte (1 TiB = 240 = 1,099,511,627,776 Byte)

`Tier` (LUN) – The technology that the disks providing the LUN are built with. (e.g. FC).

`Type` (Port) – The type of port in the array (e.g. GbE or FC).

`Manufacturer` (Disk) – This is the company that made the disk.

`Unallocated Capacity` (Array) – The total size of all storage available for host use but not currently assigned.

`Use` (Disk) – The purpose a disk is put to within an array (e.g. data or spare).

`Usable Capacity` (Array) – The total amount of storage that is available to configure from an array after system overheads – this can include virtualised capacity from an attached subordinate array.

`Used Capacity` (Pool) – This is the amount of storage within a pool that is assigned to a host as storage.

`Vendor` (Array) – This is the producer of the array.

`WWN` (Array, Port, Host) – This is the World Wide Name of the array, port or host HBA.
