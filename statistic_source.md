# Statistic Source #

This document will detail how each report element is calculated and the source of the underlying data. 

## Reports ##

* [Essentials] [report_essentials]
* [Discovery] [report_discovery]
* [Data Migration & Consolidation] [report_data-migration-&-consolidation]
* [Chargeback & Showback] [report_chargeback-&-showback]
* [Forecasting and Trending] [report_forecasting-and-trending]

## Collections ##

* [array] [collection_array]
* [collection] [collection_collection]
* [disk] [collection_disk]
* [host] [collection_host]
* [lun] [collection_lun]


## Statistic Source ##

**EMC**

* [VMAX_SYMMETRIX] [source_vmax_symmetrix] 
* [VNX_CLARIION_BLOCK] [source_vnx_clariion_block]
* [VNX_CLARIION_FILE] [source_vnx_clariion_file]

**HDS**

* [AMS_HUS_USP_VSP] [source_ams_hus_usp_vsp]

**HP**

* [EVA] [source_eva]

**IBM**

* [DSENT] [source_dsent]
* [DSMID] [source_dsmid]
* [NSERIES] [source_nseries]
* [SVC] [source_svc]
* [STORWIZE] [source_storwize]
* [XIV] [source_xiv]

**NetApp**

* [FAS7MODE] [source_fas7mode]


****************

### Essentials ###

Win business by showing the value of your assessment.

#### Summary Section ####

![report_essentials_summary.png](Images/report_essentials_summary.png "Essentials Report - Summary Items")

**Statistic Descriptions**

##### Arrays #####

    Title:              Arrays
    Description:        Total number of arrays
    Type:               Numeric
    Formula:            Count of array.array_ref_id
    DB collections:     array
    Source Statistics:  array.array_ref_id

##### Active Arrays #####

    Title:              Active Arrays
    Description:        Total number of active arrays
    Type:               Numeric
    Formula:            Count of array.array_ref_id
    DB collections:     array
    Source Statistics:  array.array_ref_id

##### Inactive Arrays #####

    Title:              Inactive Arrays
    Description:        Total number of inactive arrays
    Type:               Numeric
    Formula:            Count of array.array_ref_id
    DB collections:     array
    Source Statistics:  array.array_ref_id

##### Host Connections #####

    Title:              Host Connections
    Description:        Total number of host connections
    Type:               Count
    Formula:            Count of host.array_ref_id
    DB collections:     array
    Source Statistics:  host.array_ref_id


##### Raw Capacity Summary #####

    Name:               Raw Capacity Summary
    Description:        Total of raw capacity
    Type:               Numeric
    Formula:            Sum of disk raw_capacity for selected arrays
    DB collections:     array
    Source Statistics:  array.array_raw_capacity


##### Formatted Capacity Pie Chart #####

    Name:               Formatted Capacity Summary
    Description:        Breakdown of formatted capacity
    Type:               Pie Chart
    Formula:            None
    DB collections:     array
    Source Statistics:  array.primary_capacity, array.reserved_capacity, array.copy_capacity, 
                        array.replication_capacity, 


****************

#### Line Items ####


![report_essentials_line.png](Images/report_essentials_line.png "Essentials Report - Line Items")

##### Array Name #####

    Title:              Array Name
    Description:        Array name
    Type:               String
    Formula:            None
    DB collections:     array
    Source Statistics:  array.array_name
    
##### Data Container #####

    Title:              Data Container
    Description:        Data container name
    Type:               String
    Formula:            None
    DB collections:     array
    Source Statistics:  array.array_name
    
##### Collection Date #####

##### Entity Groups #####

##### Serial Number #####

##### Vendor #####

##### Family #####

##### Model #####

##### WWN #####
##### Raw Capacity #####
##### Disks #####
##### Ports #####
##### Host Connections #####
##### Capacity - Raw #####
##### Capacity - Usable #####
##### Capacity - Primary #####
##### Capacity - Allocated #####
##### Capacity - Replication #####
##### Capacity - Free Raw #####
##### Capacity - Unallocated #####
##### Capacity - Local Copy #####
##### Capacity - Reserved #####
##### Capacity - Installed Memory #####

*** Database Collections Used ***

* [array] [collection_array]
* [collection] [collection_collection]
* [disk] [collection_disk]
* [host] [collection_host]
* [lun] [collection_lun]



****************

### Discovery ###

A super-fast and credible snapshot of your customer's infrastructure.

**ToDo**

*** Database Collections Used ***

* [array] [collection_array]
* [collection] [collection_collection]
* [disk] [collection_disk]
* [host] [collection_host]
* [lun] [collection_lun]

****************

### Data Migration & Consolidation ###

Meet migration project timescales, demonstrate success and provide peace of mind. Timely, low-effort preparation to deliver improved data quality and security.

**ToDo**

*** Database Collections Used ***

* [array] [collection_array]
* [collection] [collection_collection]
* [disk] [collection_disk]
* [host] [collection_host]
* [lun] [collection_lun]

****************

### Chargeback & Showback ###

Generate regular business with monthly services. Fast, low-cost reporting with minimal effort.

**ToDo**


****************

### Forecasting and Trending ###

A view of the future: IT usage trend reports that predict upcoming needs. Deliver compelling proposals built on robust data.

**ToDo**

****************

## Database Collections ##

### array ###

```
{ 
    "_id" : ObjectId("5710c431f91c88780d8b4664"), 
    "unique_customer_id" : "1Gt6arzOd7Yx4KubSsElHIDedclNGpRx", 
    "array_ref_id" : "1312ceee2f", 
    "hidden" : true, 
    "collection_details" : {
        "collection_date" : ISODate("2015-07-08T12:00:00.000+0000"), 
        "collection_name" : "EVA", 
        "collection_id" : "QFkwu", 
        "upload_date" : ISODate("2016-04-15T10:36:27.000+0000"), 
        "upload_file" : "/uploads/EVA.zip", 
        "array_name" : "LHREVACDR01"
    }, 
    "serial_number" : "GB80645257", 
    "array_model" : "HSV210-02", 
    "manifest_array_name" : "LHREVACDR01", 
    "array_name" : "LHREVACDR01", 
    "data_container_unique_id" : "1Gt6arzOd7Yx4KubSsElHIDedclNGArxa", 
    "data_container_name" : "Test container", 
    "cli_version" : "10.0.0", 
    "firmware_version" : "6220", 
    "installed_memory" : NumberLong(4194304), 
    "wwn" : "5000-1FE1-500A-FBD0", 
    "vendor" : "HP", 
    "family" : "EVA", 
    "end_of_life_date" : ISODate("2008-05-31T00:00:00.000+0000"), 
    "end_of_life_date_label" : ISODate("2008-05-31T00:00:00.000+0000"), 
    "end_of_support_date" : ISODate("2013-05-31T00:00:00.000+0000"), 
    "end_of_support_date_label" : ISODate("2013-05-31T00:00:00.000+0000"), 
    "power_consumption" : 1.0, 
    "floorspace" : 0.16, 
    "heat_output" : 1000.0, 
    "rackspace" : 1.0, 
    "raw_capacity" : 195312500000.0, 
    "free_raw_capacity" : NumberLong(0), 
    "formatted_capacity" : NumberLong(195312500000), 
    "reserved_capacity" : NumberLong(0), 
    "usable_capacity" : NumberLong(195312500000), 
    "allocated_capacity" : NumberLong(125312172032), 
    "unallocated_capacity" : NumberLong(31110836224), 
    "primary_capacity" : NumberLong(125312172032), 
    "local_copy_capacity" : NumberLong(0), 
    "replication_capacity" : NumberLong(0), 
    "port_count" : NumberLong(8), 
    "disk_count" : NumberLong(200), 
    "pool_count" : NumberLong(2), 
    "lun_count" : NumberLong(179), 
    "host_count" : NumberLong(47), 
    "storage_type" : "pool", 
    "ports" : [
        {
            "port_id" : "1", 
            "adapter_id" : "7B", 
            "type" : "FibreChannel", 
            "speed" : "4", 
            "wwn" : "50001FE1500AFBDC"
        }    ], 
    "pools" : [
        {
            "id" : "00010710B40805600B52050000F0000000000400", 
            "name" : "LHREVACDR01DG01", 
            "raid_type" : "N/A", 
            "subscription_ratio" : NumberLong(100), 
            "subscribed_capacity" : NumberLong(112657334272), 
            "configured_capacity" : NumberLong(128892715008), 
            "used_capacity" : NumberLong(112657334272), 
            "free_capacity" : NumberLong(16235380736), 
            "purpose" : "basic"
        }
    ], 
    "entity_groups" : [

    ]
}
```

*** Statistic Sources ***

* [EMC VMAX_SYMMETRIX] [source_vmax_symmetrix] 
* [EMC VNX_CLARIION_BLOCK] [source_vnx_clariion_block]
* [EMC VNX_CLARIION_FILE] [source_vnx_clariion_file]

* [HDS AMS_HUS_USP_VSP] [source_ams_hus_usp_vsp] 

* [HP XIV] [source_xiv] 

* [IBM DSENT] [source_dsent] 
* [IBM DSMID] [source_dsmid] 
* [IBM NSeries] [source_nseries] 
* [IBM SVC] [source_svc] 
* [IBM Storwize] [source_storwize] 
* [IBM XIV] [source_xiv] 

* [Netapp FAS7MODE] [source_fas7mode] 

****************

### collection ###

****************

### disk ###

****************

### host ###

****************

### lun ###


****************

## Statistic Source ##

### EMC ###

* [VMAX_SYMMETRIX](source_emc_vmax_symmetrix.md) 
* [VNX_CLARIION_BLOCK](source_emc_vnx_clariion_block.md)
* [VNX_CLARIION_FILE](source_emc_vnx_clariion_file.md)

### HDS ###

* [AMS_HUS_USP_VSP](source_hds_ams_hus_usp_vsp.md)

### HP ###

* [EVA](source_hp_eva.md)

### IBM ###

* [DSENT](source_ibm_dsent.md) 
* [DSMID](source_ibm_dsmid.md)
* [NSERIES](source_ibm_nseries.md)
* [SVC](source_ibm_svc.md)
* [STORWIZE](source_ibm_storwize.md)
* [XIV](source_ibm_xiv.md)


### NetApp ###

* [FAS7MODE](source_netapp_fas7mode.md)


****************

[report_essentials]: statistic_source.md#markdown-header-essentials "Link to Essentials report"
[report_discovery]: statistic_source.md#markdown-header-discovery "Link to Discovery report"
[report_data-migration-&-consolidation]: statistic_source.md#markdown-header-data-migration-&-consolidation "Link to Data Migration & Consildation report"
[report_chargeback-&-showback]: statistic_source.md#markdown-header-chargeback-&-showback "Link to Chargeback & Showback report"
[report_forecasting-and-trending]: statistic_source.md#markdown-header-forecasting-and-trending "Link to Forecasting and Trending report"

[collection_array]: statistic_source.md#markdown-header-array "Link to array collection"
[collection_collection]: statistic_source.md#markdown-header-collection "Link to collection collection"
[collection_disk]: statistic_source.md#markdown-header-disk "Link to disk collection"
[collection_host]: statistic_source.md#markdown-header-host "Link to host collection"
[collection_lun]: statistic_source.md#markdown-header-lun "Link to lun collection"

[source_vmax_symmetrix]: statistic_source.md#markdown-header-vmax_symmetrix "Link to EMC VMAX/Symmetrix source document"
[source_vnx_clariion_block]: statistic_source.md#markdown-header-vnx_clariion_block "Link to EMC VMAX/CLARiiON block source document"
[source_vnx_clariion_file]: statistic_source.md#markdown-header-vnx_clariion_file "Link to EMC VMAX/CLARiiON file source document"
[source_ams_hus_usp_vsp]: statistic_source.md#markdown-header-source_ams_hus_usp_vsp "Link to HDS AMS/HUS/USP/VSP source document"
[source_eva]: statistic_source.md#markdown-header-source_xiv "Link to HP EVA source document"
[source_dsent]: statistic_source.md#markdown-header-source_dsent "Link to IBM DS Enterprise source document"
[source_dsmid]: statistic_source.md#markdown-header-source_dsmid "Link to IBM DS modular source document"
[source_nseries]: statistic_source.md#markdown-header-source_nseries "Link to IBM N-Series source document"
[source_svc]: statistic_source.md#markdown-header-source_svc "Link to IBM SVC source document"
[source_storwize]: statistic_source.md#markdown-header-source_storwize "Link to IBM Storwize source document"
[source_xiv]: statistic_source.md#markdown-header-source_xiv "Link to IBM XIV source document"
[source_fas7mode]: statistic_source.md#markdown-header-source_fas7mode "Link to NetApp FAS (7 mode) source document"

[Top] (statistic_source.md#markdown-header-statistic-source)