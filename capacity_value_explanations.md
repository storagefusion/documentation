# Capacity Calculation Definitions

### Lowest Level of Abstraction
##### * Raw Capacity

  - Raw Capacity = (A1 x B1) + (A2 x B2) + ...
Where:  
    - A = Capacity of disk type as quoted by manufacturer (stored in our hardware database).
    - B = Number of disks installed of that type.

Repeat for each discrete disk type installed - the array raw capacity will be the sum of all its component disks.

    - Relationship: Raw Capacity = Formatting Losses + Formatted Capacity + Free Raw Capacity

### 2nd Level of Abstraction
##### * Free Raw Capacity

  - Free Raw Capacity = (A1 x B1) + (A2 x B2) + ...
Where:  
    - A = Capacity of disk type as quoted by manufacturer (stored in our hardware database).
    - B = Number of disks installed that are not configured as spare or in use of that type.

Repeat for each discrete disk type installed - the array free raw capacity will be the sum of all the unused disk raw capacities. 

    - Relationship: Free Raw Capacity is a subset of Raw Capacity only.

##### * Formatted Capacity

  - Formatted Capacity = (A1 x B1) + (A2 x B2) + ...+ C
Where:  
    - A = Formatted capacity of disk type as quoted by array collection.
    - B = Number of disks of that type that are configured as spare, reserved, or are in use.
    - C = Virtualised capacity presented to array.

Repeat for each discrete disk type installed - the formatted capacity will be the sum of all the used and spare disk formatted  capacities. 

    - Relationship: Formated Capacity is a subset of Raw Capacity.
    - Relationship: Formatted Capacity = Usable Capacity + System Overheads

##### * Formatting Losses
- Formatting Losses = Raw Capacity - (Free raw Capacity + Formatted Capacity)

    
    - Relationship: Formatting Losses are a subset of Raw Capacity.
    

##### * Virtualised Capacity IN
- Virtualised Capacity IN = Virtualised input Capacity as collected from recieving array.

##### * Virtualised Capacity OUT
- Virtualised Capacity OUT = Virtualised output Capacity as collected from giving array.

### 3rd Level of Abstraction
##### * System Overheads

  - System Overheads = (A1 x B1) + (A2 x B2) + ...
Where:  
    - A = Formatted capacity of disk type as quoted by array collection.
    - B = Number of disks of that type that are configured as spare or reserved.

Repeat for each discrete disk type installed - the system overheads will be the sum of all reserved and spare disk formatted capacities. 

    - Relationship: System overheads are a subset of Formatted Capacity.

##### * Usable Capacity

  - Usable Capacity = (Formatted Capacity - System Overheads) + Virtualised Capacity IN - Virtualised Capacity OUT


    - Relationship: Usable Capacity = Allocated Capacity + Unallocated Capacity + Free Capacity.

### 4th Level of Abstraction
##### * Allocated Capacity

  - Allocated Capacity = (LUN Configured Size1 + LUN Configured Size2 + ...)
Where:  
    - LUN Configured size = Configured size of LUN with path to a Host.
   

    - Relationship: Allocated Capacity is a subset of Usable Capacity.
    - Relationship: Allocated Capacity = Primary Capacity + Replication Capacity + Copy Capacity
(Where Primary Capacity is LUN configured Size for all LUNs of type=Primary with connections to hosts - and so on).

##### * Unallocated Capacity

  - Unallocated Capacity = (LUN Configured Size1 + Lun Configured Size2 + ...)
Where:  
    - LUN Configured size = Configured size of LUN without path to a Host.


    - Relationship: Unallocated Capacity = Primary Capacity + Replication Capacity + Copy Capacity.
    - Relationship: Unallocated Capacity is a subset of Usable Capacity.
(Where Primary Capacity is LUN Configured Size for all LUNs of 'type = Primary' without connections to hosts - and so on).

##### * Free Capacity

  - Free Capacity = Reserved Capacity + (Usable Capacity - (Allocated Capacity + Unallocated Capacity))
Where:  
    - Reserved Capacity = free space within storage pools.


    - Relationship: Free Capacity is a subset of Uasable Capacity.

### 5th Level of Abstraction
##### * Primary Capacity

  - Primary Capacity = Primary Capacity1 + Primary Capacity2 
Where:  
    - Primary Capacity1 is LUN Configured Size for all LUNs of type=Primary with paths to hosts.
    - Primary Capacity2 is LUN Configured Size for all LUNs of type=Primary without paths to hosts.
   

    - Relationship: Primary Capacity is a subset of Allocated and Unalloacted Capacity.
  
##### * Replication Capacity

  - Replication Capacity = Replication Capacity1 + Replication Capacity2
Where:  
    - Replication Capacity1 is LUN Configured Size for all LUNs of type=Replication with paths to hosts.
    - Replication Capacity2 is LUN Configured Size for all LUNs of type=Replication without paths to hosts.


    - Relationship: Replication Capacity is a subset of Allocated and Unalloacted Capacity.


##### * Local Copy Capacity

  - Local Copy Capacity = Local Copy Capacity1 + Local Copy Capacity2
Where:  
    - Local Copy Capacity1 is LUN Configured Size for all LUNs of type=Copy with paths to hosts.
    - Local Copy Capacity2 is LUN Configured Size for all LUNs of type=Copy without paths to hosts.


    - Relationship: Local Copy Capacity is a subset of Allocated and Unalloacted Capacity.