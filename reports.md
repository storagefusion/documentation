# Customer Reports

##Essentials (core executive summary)
#### Summary
A single-pane-of-glass monitor for your heterogeneous storage estate highlighting the essential KPIs of your environment – allowing you to manage and highlight day-to-day capacity issues.
The essentials report shows storage totals and utilisation figures for all supported arrays and vendors.

#### Detail
This report shows the latest summary data for each uploaded array in your estate up until a date you specify.
The summary consists of:

- Total number of arrays (split into active and inactive).
- Total number of host connections.

This report includes a pie chart summary of the estate showing percentage and capacity for raw, unallocated, reserved, Primary, Copy, and Replication storage.
A search facility is provided to allow you to filter the results by location right down to individual array.

The detail of this report includes specific array information:

- The number of internal disks, front end ports, pools, and host connections in each array.
- Array details including: CLI/Firmware versions; WWN; Serial; for each array.
- Array capacity information in TiB including: Raw; Usable; Unallocated; Primary; Copy; Allocated; Reserved; Replication; Cache;
- Ability to associate assets with user customised entities (e.g. business area or location).

#### Content (All filerable by Vendor/Family/Array Name/Model/SerialNumber/CLI Version/Firmware Version/WWN/DataContainer/EntityGroup/Nodes): 
#####Top
- Array Count
- Active/Inactive Array Count
- Host Connections Count
- Total Raw Capacity
- LUN Formatted Capacity Usage (Unallocated, Primary, Copy, Replication).
- File System Formatted Capacity Usage (Unallocated, Primary, Copy, Replication).

#####Row
Table of: (with Export function)

- Array Name
- Data Container
- Collection Date
- Serial Number
- Vendor
- Family
- Model
- WWN
- Raw Capacity

#####Expanded

- Count of Disks
- Count of Ports
- Count of Pools
- Count of LUNs
- Count of File Systems
- Count of Host connections
- Firmware Version
- CLI Version
- Entity Groups
- Array Raw Capacity
- Array Free Raw Capacity
- Array Installed Memory
- Array Usable
- Array Allocated
- Array Unallocated
- LUN Primary
- LUN Local Copy
- LUN replication
- LUN Reserved
- FS Primary
- FS Local Copy
- FS replication
- FS reserved




## Discovery (& Audit)
#### Summary
A detailed under the covers report of your whole storage environment – showing you the detailed technology breakdown, the full LUN view from LUN to provider, capacity function (replication, copy, primary).
The perfect solution to in depth review and audit of your storage components allowing you trace host WWNs, LUNs – from your customer perspective importing names, business owner, asset tags to be imported and assigned to your discovered nodes.
Including an end of service life report allows you to see which devices in your estate are falling from support in the coming months and years to allow you to factor their capacity into your replacement policy.

#### Detail

This report shows the latest summary data for each uploaded array in your estate up until a date you specify.

The summary consists of:

- Total number of LUNs (split by purpose – Primary, Replication, and Copy).
- Total estate size in TiB of raw capacity.

A series of charts showing:

- Raw capacity by array vendor as a percentage of the estate or selected devices.
- Allocated capacity by End of Service Life (showing capacity on expiring devices at 6 month intervals) as a percentage of the estate or selected devices.
- A chart showing allocation by purpose as a percentage and TiB of the estate or selected devices (primary, copy, replication, unallocated, reserved).

A search facility is provided to allow you to filter the results by location right down to individual array.

The ability to associate assets (Arrays, and Hosts) with user customised entities (e.g. business area or location).

Detailed information from each array:

- Charts for raw capacity by technology (SSD, FC, SATA etc.) in TiB.
- Capacity usage by purpose in TiB.
- Attached host information (Including – Name, WWN, Assigned Capacity).
- Port information (Including - Adapter ID, Type, Speed, and WWN).
- Disk information (Including – Serial, Manufacturer, Model, Technology, Raw Capacity,Formatted Capacity, Location, Function, and Power Use).
- Pool information (Including – Name, Function, RAID type, Configured Capacity, Used Capacity, - Free Capacity, Subscribed Capacity, Subscription Ratio).
- LUN information (Including – ID, Name, Purpose, Provider, RAID type, Provider Technology, Configured Capacity TiB, Subscribed Capacity TiB, Features (e.g. Thin), Assigned Host Map.

#### Content (All filerable by Vendor/Family/Array Name/Model/SerialNumber/CLI Version/Firmware Version/WWN/DataContainer/EntityGroup/Nodes/End of Support Date): 
#####Top
- Primary LUN Count.
- Replication LUN Count
- Copy LUN Count
- Total Raw Capacity
- Primary File System Count
- Replication File System Count
- Copy File System Count
- Chart of Raw Capacity by Vendor (Percentage of total Data Container Raw Capacity, and TiB capacity).
- Chart of EOSL Breakdown by Allocated Capacity (Percentage of total Data Container Allocated Capacity, and TiB capacity).
- Chart of Raw Capacity by Technology (Percentage of total Data Container Raw Capacity, and TiB capacity)
- Chart of LUN Allocated Capacity Usage (Primary, Copy, Replication, Reserved - Percentage of total Data Container LUN Allocated Capacity, and TiB capacity).
- Chart of File System Allocated Capacity Usage (Primary, Copy, Replication, Reserved - Percentage of total Data Container File System Allocated Capacity, and TiB capacity).

#####Row
Table of: (with Export function)

- Array Name
- Data Container
- Collection Date
- Serial Number
- Vendor
- Family
- Model
- End of Support Date
- Raw Capacity

#####Expanded

- Chart of Disk Technology breakdown for Array (percentage, and capacity).
- Count of Disks
- Details of Disks - Serial Number, Manufacturer, Model, Technology, Speed, Power Use, Location, Function, Raw and Formatted Capacity.
- Count of Ports
- Details of Ports - Port Number, Adapter Id, Type, Speed, WWN.
- Count of Pools
- Details of Pools - Pool Name, Purpose, RAID type, Subscription ratio, Configured capacity, Used Capacity, Free Capacity, Subscribed Capacity.
- Count of LUNs
- Details of LUNs - LUNID, LUN Name, Function, Provider, RAID Type, Tier, Thin Prov, Configured and Subscribed size.
- Count of File Systems
- Details of File Systems - Name, Provider, RAID Type, Tier, Thin Prov, Configured and Subscribed Size.
- Count of Host connections
- Details of Host connections - Host connection name, WWNs of host connection, IP Address.
- Firmware Version
- CLI Version
- Entity Groups
- Array Raw Capacity
- Array Free Raw Capacity
- Array Installed Memory
- Array Usable
- Array Allocated
- Array Unallocated
- LUN Primary
- LUN Local Copy
- LUN replication
- LUN Reserved
- FS Primary
- FS Local Copy
- FS replication
- FS reserved

## Charge-back & Show-back

#### Summary
A consumer viewpoint of your storage environment – allowing you to evidence and demonstrate top consumers and growth areas – proving you the details behind monthly growth patterns to identify contributing business areas and regions so you can target unplanned capacity changes.
This report will allow you to put a real per tier cost on your own storage services to demonstrate and help secure funding for capacity changes or to support billing services for hosted environments.

#### Detail

This report shows the latest summary data for each uploaded array in your estate between the dates you specify.

The summary consists of:

- The total size of all subscribed LUNs in your estate at each tier for the start and end dates in TiB.
- The difference between the total subscribed LUN size for your estate between these two dates showing you estate growth by tier as a percentage change.
- A summary line chart showing you how the LUN subscribed size has varied through each of your collection points.

A search facility is provided to allow you to filter the results by location right down to individual array or host.
 The ability to associate assets (Arrays, and Hosts) with user customised entities (e.g. business area or location).

Detailed statistics and information are provided on:

- Host information – including LUN subscribed allocation at the host and tier level between the reporting dates, highlighting the growth at each technology tier of each host as both a percentage and TiB value.
- Host specific details – including Name, IP Address, WWN, Allocated LUNs.

#### Content (All filerable by Vendor/Family/Array Name/Model/SerialNumber/CLI Version/Firmware Version/WWN/DataContainer/EntityGroup/Nodes/End of Support Date): 
#####Top
- Table of LUN subscribed size technology Composition change between dates.
- Chart of total LUN subscribed size changes between dates.

#####Row
- Host Connection Name
- Data Container
- Host Connection IP
- LUN Subscribed capacity at start date (and the associated collection date).
- LUN Subscribed capacity at end date (and the associated collection date).
- LUN Subscribed change between the dates
- LUN Subscribed ratio between the dates

#####Expanded
- Host Connection WWN list
- Host LUN Technology subscribed capacity breakdown for Start date.
- Host LUN Technology subscribed capacity breakdown for End date.
- Host LUN Technology subscribed capacity change between dates.
- Host LUN Technology ratio change between dates.
- List of Host associated LUNs on Start date (Count of LUNs, LUNID, Provider Array Name, LUN Name, LUN Type, Provider, RAID Type, Technology Tier, Configured size, Subscribed size, Thin Provisioned).
- List of Host associated LUNs on End date (Count of LUNs, LUNID, Provider Array Name, LUN Name, LUN Type, Provider, RAID Type, Technology Tier, Configured size, Subscribed size, Thin Provisioned).

## Data Migration & Consolidation (what if)

#### Summary
A migration planning and consolidation tool – this facility will support your plans for migration at either host or array level – showing host to array affinity and real capacity requirements. It will allow you to generate details of LUNs or hosts that can be used as input into your actual migration execution.
This function provides details on the target environments to show you where in your estate you can migrate the source capacity – informing your decisions and allowing you to simulate ‘what if’ scenarios of company migration or consolidation after acquisition, individual array clear-down or optimisation, or targeted business area host or LUN movement.

#### Detail

This report shows the latest summary data for each uploaded array in your estate between the dates you specify.

The summary consists of:

- LUN quantity and subscribed sizes in TiB by technology tier.
- Number of host connections.
- Number of arrays.

A search facility is provided to allow you to filter the results by location right down to individual array or host. 
The ability to associate assets (Arrays, and Hosts) with user customised entities (e.g. business area or location).

Detailed statistics and information are provided on an array by array basis for:

- Configured LUNs– (Including – ID, Name, Configured Capacity, Subscribed Capacity, Purpose, Provider, RAID Type, Features (e.g. Thin), Attached Hosts).
- Connected Hosts – (Including – Name, WWN, Allocated Capacity).
- Relationships between hosts, LUNs, and providing arrays.
- User defined entities (Business area, region, service levels etc.)

#### Content (All filerable by Vendor/Family/Array Name/Model/SerialNumber/CLI Version/Firmware Version/WWN/DataContainer/EntityGroup/Nodes/End of Support Date): 
#####Top
- Array Count
- File System Count
- LUN Count
- LUN to Host Path Count
- Table of File System Compostition by Technology Type (TiB)
- Table of LUN Compostition by Technology Type (TiB)

#####Row
Two Tables of: (with Export function)

#####LUNs (Tab with LUN count)
- LUN ID 
- Collection Date
- LUN Name
- Configured Size
- Subscribed Size
- Type
- RAID Type
- Thin Provisioned
- Data Container 
- LUN to Host path count

#####File Systems (Tab with File System Count)
- File System Full Path Name
- Collection Date
- Configured Size (TiB)
- Subscribed Size (TiB)
- Type
- RAID Type
- Thin Provisioned
- Data Container

#####Expanded - LUN Row
- Providing Array Name
- Providing Array End of Support Date
- Providing Array Firmware Level
- Providing Array WWNN
- Providing Array Unallocated Capacity (TiB)
- Providing Array Entity Groups
- Details of Hosts attached to LUN (hostname, WWN, IP Address)

#####Expanded - File System Row
- Providing Array Name
- Providing Array End of Support Date
- Providing Array Firmware Level
- Providing Array WWNN
- Providing Array Unallocated Capacity (TiB)
- Providing Array Entity Groups

## Forecasting & Trending

#### Summary
A great aid to your automated capacity planning for your storage environment – where are your hot spots for capacity, which business areas have experienced most growth or decline at node, device, region or entire estate level. Where your optimum usage is over your defined reporting period are your business plans borne out by real changes in your storage metrics.
Showing you estate deltas at all levels, and business functions, at time and date scales defined by your business for evidence based capacity plans.
Forecasting will allow you to see when consumption will outstrip capacity – in pool, array, business area, or region – supporting the seamless function and provisioning of your estate no matter how big or small, diverse, or distributed.

#### Detail

This report shows the latest summary data for each uploaded array in your estate between the dates you specify.

The ability to associate assets (Arrays, and Hosts) with user customised entities (e.g. business area or location).

The summary consists of:

- Whole estate monthly allocated capacity growth rates (TiB and %).
- Number of arrays.
- Number of host connections.
- Whole estate monthly raw capacity growth rates (TiB and %).
- A series of charts showing estate capacities and how they have varied over the past 12 months by purpose and function.

Detailed statistics and information are provided on:

- Projected dates for occupation maximum (full tiers) based on all data.
- Projected dates for occupation maximum (full tiers) based on reporting dates.
- Number of days for each tier to reach capacity at current growth rates.
- Percentage and capacity growth rates for each array between selected dates.
- Charts showing how selected array capacities have varied between the selected dates by purpose and function.
- Selected array allocated LUN details.
- Selected array connected Host details.

#### Content (All filerable by Vendor/Family/Array Name/Model/SerialNumber/CLI Version/Firmware Version/WWN/DataContainer/EntityGroup/Nodes/End of Support Date): 
#####Top
- Count of Arrays
- Count of Host Connections
- Monthly change in Raw Capacity (TiB and percentage)
- Monthly change in Total Allocated Capacity (TiB and percentage)
- Monthly change in Total LUN Allocated Capacity (TiB and percentage)
- Monthly change in Total File System Allocated Capacity (TiB and percentage)
- Chart of Total Capacity (Raw, Free raw, Usable, Allocated, Unallocated) changes over chosen period.
- Chart of Total LUN Capacity (LUN Allocated, LUN Primary, LUN Local Copy, LUN Reserved, LUN Replication) changes over chosen period. 
- Chart of Total File System Capacity (F/S Allocated, F/S Primary, F/S Local Copy, F/S Reserved, F/S Replication) changes over chosen period.

#####Row - List of Arrays
- Array Name
- Data Container
- Serial Number
- Collection Date
- Estimated Full Capacity Date

#####Expanded
- Array Name
- Array End of Support Date
- Array End of Life Date
- Array Entity Groups
- Array Capacity Information - Total Allocated Capacity at Start and End dates, Change (TiB and percentage).
- Array Capacity Information - Total UnAllocated Capacity at Start and End dates, Change (TiB and percentage).
- Array Capacity Information - Total Raw Capacity at Start and End dates, Change (TiB and percentage).
- Array Capacity Information - Total Free Raw Capacity at Start and End dates, Change (TiB and percentage).
- Array Capacity Information - Total Usable Capacity at Start and End dates, Change (TiB and percentage).
- Chart of Array Total Capacity History (Raw, Free Raw, Usable, Allocated, Unallocated).
- List of Array Total Capacity values for all collections between the selected dates (Raw, Free Raw, Usable, Allocated, Unallocated).
- List of Array LUN Capacity values for all collections between the selected dates (Raw, Free Raw, Usable, Allocated, Unallocated).
- List of Array File System Capacity values for all collections between the selected dates (Raw, Free Raw, Usable, Allocated, Unallocated).
- List of Array LUNS (LUN ID, LUN Name, Type, Provider, RAID Type, Tier, Thin Provisioned, Configured Size, Subscribed Size).
- List of Array File Systems (LUN ID, LUN Name, Type, Provider, RAID Type, Tier, Thin Provisioned, Configured Size, Subscribed Size).
- List of Array Host Connections (Name, WWN, IP Address).

## Tiering & Performance

#### Summary

The operational assist tool that lets you combine physical with virtual configuration layers to identify performance bottlenecks and monitor the through-put of your environment.
Identify single points of failures and risks in your storage configuration – allowing any potential performance threats to be resolved before they impact the business.
This report pack will look at your use of the available tiers in your configuration and will inform you of potential economies to be made by re-tiering or tuning in sub-optimal environments.

#### Detail

This report shows the latest summary data for each uploaded array, host, and switch in your estate between the dates you specify.

The ability to associate assets (Arrays, and Hosts) with user customised entities (e.g. business area or location).

The summary consists of:

- LUN quantity and subscribed sizes by tier.
- Average LUN throughput Minimum/Maximum/Average LUN in KBps (split between read and write).
- Average Storage path latency Minimum/Maximum/Average in ms (split between read/write).

A search facility is provided to allow you to filter the results by location right down to individual array, host, or LUN.

Detailed statistics and information are provided on:

- Individual LUN details and information (LUN throughput Minimum/Maximum/Average LUN in KBps (split between read and write).
- Individual Host details (average host LUNs throughput Minimum/Maximum/Average LUN in KBps (split between read and write) – Host CPU and Memory statistics should be included too.
- Individual Array details (Array throughput Minimum/Maximum/Average LUN in KBps (split between read and write).
- Individual Switch details (Switch and port throughput Minimum/Maximum/Average LUN in KBps).
- Details of ISLs – relationships (capacity and usage).
- Relationships between hosts, LUNs, and providing arrays.
- User defined entities (Business area, region, service levels etc.)
- Tier usage at estate level – allowing you to see occupancy levels – recommendations to increase tiering or move LUNs based on IO rates.


## Data Protection & Recovery

#### Summary

This report set will allow you to review the state of your backup and recovery processes – reviewing the backup logs from our supported product list takes time - let us highlight any issues and allow you to focus on resolving them.
How much capacity are your backups using – are there any LUNs attached to your hosts that are not following the correct company policy.
This report will allow you to discover sleeping problems in your environment so they can be cleared before they impact your business.

#### Details

This report shows the latest summary data for each uploaded array, host, and switch in your estate between the dates you specify.

The ability to associate assets (Arrays, and Hosts) with user customised entities (e.g.
business area or location).

The summary consists of:

- LUN quantity and subscribed sizes for Primary/Copy/Replication.
- Number of active Replication relationships.
- Number of active Copy relationships.
- Total capacity of backup copies.
- Number of alerts/errors for backup functions.

A search facility is provided to allow you to filter the results by location right down to individual array, LUN or host. 
Detailed statistics and information are provided on:

- Individual LUN details and information.
- Individual Host details.
- Relationships between hosts, LUNs, and providing arrays.
- User defined entities (Business area, region, service levels etc.)
- Details of any alerts/errors found for backups.
- Details of all replication pairs and relationships.
- Details of all copy pairs and relationships.
- Details of any tape robotics – capacity – model – drives – used/scratch capacity split.

## Storage Efficiency (Deduplication & Compression)

#### Summary

This report will allow you to see how efficiently you are using your storage – do you have duplication or compression configured. How much storage are these technologies saving you – these reports present physical storage use alongside deduplication and compression savings.
Showing these features alongside the Host, LUN and array capacity statistics and information.

#### Details

This report shows the latest summary data for each uploaded array, host, and switch in your estate between the dates you specify.

The ability to associate assets (Arrays, and Hosts) with user customised entities (e.g. business area or location).

The summary consists of:

- Estate capacity summary – Raw/Allocated/Used/Dedupe saving/Compression saving.
- Number of host connections.
- Number of arrays.

A search facility is provided to allow you to filter the results by location right down to individual array, host, or LUN. 
Detailed statistics and information are provided on:

- Individual LUN details and information – including compression and dedupe savings.
- Individual Host details – including features of any attached storage – thin/comp/dedupe.
- Relationships between hosts, LUNs, and providing arrays.
- User defined entities (Business area, region, service levels etc.)

## Configuration Management (SANview)

#### Summary

Map and view relationships between hardware configuration items and services hosted.
This allows you to perform problem determination at a physical level for virtual layer problems – see virtual guest matched to host, host to fibre port, and fibre port to array resources.
The reports include visual graphics with on screen with filters that can be set from any component perspective.
The import of CSV type CMDB or business/service/tenant data allows these reports to be used in a change management or service management situation too.
“I am authorising an upgrade or planning a change what services are affected?  Which business units need to sign this off?”


#### Details

Configuration Management (SANview)
This report shows the latest summary data for each uploaded array, host, and switch in your estate between the dates you specify.

The ability to associate assets (Arrays, and Hosts) with user customised entities (e.g. business area or location).

The summary consists of:

- Number of array connections.
- Number of host connections.
- Number of switch connections.:
- Number of hosts.
- Number of Arrays.
- Number of switches.

A search facility is provided to allow you to filter the results by location right down to individual array, host, switch, or business area/service. 
Detailed statistics and information are provided on:

- Individual LUN details and information.
- Individual array details (including FED port maps).
- Individual switch details (including port maps).
- Relationships between hosts, LUNs, switches, and providing arrays.
- User defined entities (Business area, region, service levels etc.)


## Health-check

#### Summary

This report allows you to perform a series of pre-defined best practice checks against the discovered and supported environment.
Are there any imbedded problems – are you making good use of the SAN resources – is the right data in the right place – do you have any reclaimable capacity - are there any features that we could be making better use of or indeed deploy.

#### Detail

This report shows the latest summary data for each uploaded array, host, and switch in your estate between the dates you specify.
The ability to associate assets (Arrays, and Hosts) with user customised entities (e.g. business area or location).

The summary consists of:

- Number of Hosts/Arrays/Switches.

- Number of Errors/Alerts/Warnings.

A search facility is provided to allow you to filter the results by location right down to individual array or host. 
Detailed statistics and information are provided on:

- Details of all errors found – lists of effected LUNs, hosts, switches identifying faulty component or configuration terms.
- Individual Host/Switch/Array/LUN details.
- User defined entities (Business area, region, service levels etc.)