### What is this repository for? ###

This repo holds all forms of documentation relating to the core-app project.

* Single source for all forms of documentation
* Documents where possible will be held in Markdown format 
  and converted as required
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

---

##### Reports & Information

* [Reports](master/reports.md)
* [Glossary of Terms](glossary_of_terms.md)
* [Hardware support](hardware_support.md)

##### Summary stats

Jira: SF-217

* `todo` [EMC VMAX]()
* `todo` [EMC Clariion Block]()
* `todo` [EMC Clariion File]()
* `todo` [HDS AMS-HUS]()
* `todo` [HDS AMS-VSP]()
* `todo` [HP 3PAR]()
* `todo` [HP EVA]()
* `todo` [HP XP]()
* `todo` [IBM DSMID]()
* `todo` [IBM DSENT]()
* `todo` [IBM NSeries]()
* `todo` [IBM SVC]()
* `todo` [IBM Storwise]()
* `todo` [IBM XIV]()
* `todo` [NETAPP FAS7MODE]()

##### Storage Concepts

![Storage concepts overview diagram](Images/array_infrastructure_overview.jpg "Data Package Structure")

* [Storage Virtualisation](storage_concepts/storage_virtualisation.md)
* [Data Collection De-duplication](storage_concepts/data_collection_de-duplication.md)

##### Array internals and capacity diagram
![Array internals and capacity diagram](Images/storage_structure.jpg "Array internals and capacity")

##### Other things
* [Capacity calculation explanations](capacity_value_explanations.md)

##### System Documentation

* [CLI Version Handling](storage_concepts/cli_version_handling.md)
* Item 2
* Item 3