# Storage Concepts

### Storage Virtualisation

#### Definition

Storage virtualization is the pooling of physical storage from multiple network storage devices into what appears to be a single storage device that is managed from a central console.

![Alt text](../Images/storage_virtualisation.jpg "Storage Virtualisation")


Storage virtualisation can be performed for many reasons.

Advantages include:

-   Single interface to manage creation and publishing of host facing storage (LUNs).

-   Single mechanism for the connection of end-user hosts, thus simplifying their configuration.

-   Back-end arrays can be swapped out without said hosts even being aware.

-   Companies are able to migrate vendors and technologies of their back-end kit without modifications to end-user systems

### Issues

#### Reporting - Over stating of estate storage capacity

With storage virtualisation a front-end array will present storage as its own regardless of whether that storage is formed from its own physical drives (if indeed it has any) or from luns provided by a back-end array.

**_Any report detailing all arrays within the estate will count the same storage twice*: once for the front-end array receiving the storage and once for the back-end array where it physically resides._**

#### Reporting - Over stating of storage end-users

All arrays present their block-based storage as luns for consumption by their users. In the case of the back-end arrays their users are not end-user hosts but other arrays. For the arrays there is no difference if the consumer of their luns is a host or an array.

**_Any report detailing hosts will include front-end arrays within its listing of end-user hosts._**

#### Reporting – Virtualisation prevents identification of raid protection and disk technology

As the front-end array just links to a provided lun, it has no more knowledge of the underlying raid protection or disk technology afforded to it than a regular host does. Disk technology is the basis of our lun tiering.

**_Any report detailing lun RAID protection will show as unknown for virtualised storage._**

**_Any report detailing lun tier as unknown for virtualised storage._**

### Array types that support storage virtualisation

The following array types are advertised as supporting storage virtualisation:

EMC - Symmetrix/VMAX with federated storage – **Not currently supported by our products**

HDS - USP, USPV & VSP

HP - XP

IBM - SVC & Storwize

Netapp - V series – **Functionally almost identical to FAS hardware but not supported by our products**

### Information required to identify storage virtualisation

In general information has to be gathered from the recipient of the storage because as stated previously, back-end arrays are not aware of any differences between host consumers and array consumers. The front-end arrays will however have knowledge of the arrays they are receiving storage from. The information they are able to report includes:

-   Identifier for front-end array

  As stated previously most information will be sourced from the front-end array so naturally its identity will be known.

-   Identifier for back-end array

    Architectures vary so the back-end array may be known, to the front-end array, by its name or WWNN.

-   Front-end identifier for Lun

    How the lun from the back-end array is incorporated into the front-end system will vary between architectures but generally they are treated as pseudo disk devices.

    This information will allow tracking of its usage and ultimately determine what host facing luns are created from it. This will allow end-to-end reporting that is to say from host to source array.

-   Rear-end identifier for Lun

    This information will allow determination of raid protection and performance tiering.

-   Lun Size

    Note: Size as seen by front-end array may differ from that stated by the back-end array due to administration overhead. Any difference in size will need to be accounted for.

### Linkage Methods

1.  Add additional keys to database collections

    **‘array’ collection**

      New key: ‘virtual_capacity’
          Integer value represents the sum, in KiB, of all virtualised storage assigned to 
          array (‘virtual_storage. virtual_size’).

      New sub-collection: ‘virtual_storage’ {
          array_ref_id: Ref id of source array
          array_serial_number: Serial number of source array
          array_wwn: Wwnn of source array
          lun_id: Id of source lun
          lun_wwn: Wwn of source lun
          virtual_size: Size of lun as seen by receiving array
          source_size: Subscribed size of lun (from source array)
          raid_type: Level of RAID protection (from source array)
          tier: Performance tier (from source array)
      }
     *Note: Typically only a sub-set of the above values will be present in the recipient array raw data and that sub-set will vary across the architecture types. A secondary process would have to fill in the missing values.*

    **‘host’ Collection**

      New key ‘host_is_array’
          Boolean – true means the host document refers to an array and false means 
          that the host is an end-user server. Default value is false.

    **‘lun’ Collction**

      New key ‘virtual’
          Boolean - true means the lun is carved from virtualised storage and false means it is carved from local physical storage.

2.  Update to ‘Process Array’ stage

    1.  Check that ‘array.raw_capacity’ does not include virtualised storage.

    2.  Population of ‘array.virtual_storage’ sub-collection from recipient array source data.

    3.  Population of ‘array.virtual_capacity’ by summing virtual_size data from ‘array.virtual_storage’.

    4.  Set ‘lun.virtual’ to true for all luns carved from virtualised storage.

3.  Add additional process to the processor chain

![Alt text](../Images/processing_sequence_virtualisation.jpg "Processing Sequence")
Note: Collection de-duplication is not currently an active process. 
It is shown purely to highlight where it would appear in the data processing sequence

The new process would be responsible for linking recipient arrays to source arrays.
This will entail, for each document in ‘array .virtual_storage’:

1.  Identify source array using information present in ‘array .virtual_storage’ sub-collection.

2.  Fill in missing source array details in ‘virtual_storage’ sub-collection .

3.  Locate receiving array in source array host collection and setting:

    1.  ‘host_is_array’ key to true.

4.  Locate virtualised lun in source array lun collection and setting :

    1.  ‘virtual_storage .source_size ’ to ‘lun.subscribed_size

    2.  ‘virtual_storage .raid_type ’ to ‘lun.raid_type

    3.  ‘virtual_storage .tier’ to ‘lun.tier

### Handling Partial Collections

#### Recipient Array Missing

If recipient arrays are not included in the collection then no further remedial work is possible. End-user hosts connected to the recipient arrays will obviously not feature in any report. The missing arrays themselves will feature in the host list for the source array but will not be identified as arrays. No virtual capacity will be identified.

#### Source Array Missing

If source arrays are not included in the collection then the ability to determine raid protection and performance tier for the storage they supply is lost. Any end-user hosts connected directly to source arrays will not feature in reports. Totals for ‘virtual_capacity’ will still be calculated correctly.

### Resolution of Issues Using New Database Values

#### Reporting - Over stating of estate storage capacity

> Report only ‘array.raw_capacity’ values. These will detail only the physical storage installed in an array.
>
> ‘array.virtual_storage’ should be added to ‘array.raw_capacity’ to report on total storage available to individual arrays.

#### Reporting - Over stating of storage end-users

> Report should exclude hosts where ‘host_is_array’ flag is set.
>
> If you wish to report on storage assigned to virtualisation then then select only hosts where ‘host_is_array’ flag is set.

#### Reporting – Virtualisation prevents identification of raid protection and disk technology

> Assuming that back-end arrays have been included in the collection then both raid type and performance tier values will have been calculated.
