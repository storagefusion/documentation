De-duplication of data collections
==================================

Overview 
---------

### De-duplication – What does that mean?

De-duplication in the context being discussed is:

**_The removal of duplicate entries that arise from taking data collections from multiple storage controllers installed within the same array_**

Note: This document is not concerned with the process of user data de-duplication that arrays perform as a means of data compression.


### Background - How array hardware is structured

**_The primary design goal for arrays is the protection of data._**

Performance is of course high on the list of requirements but even that is secondary to data protection. Nobody would be willing to risk their company by buying storage arrays that were lightning fast but couldn’t keep their data safe.

So how do array manufacturers go about ensuring their arrays do the best possible job in keeping data safe? The answer is redundancy.

**_Anything mechanical can and WILL go wrong at some point in time._**

The trick is designing a system that can cope with components failing at least long enough to give an engineer the time required to fix it. Array designers strive to ensure that there is no *single point of failure* within an array.

This concept of redundancy is applied at many levels.

No component exists as a one-off. There will be multiple disks, disk controllers, I/O controllers, power supplies, etc.

Entire storage controllers (the brains of the array) are replicated as well. There may well be two, four or even eight instances in a clustered design.

In addition to redundancy considerations, array designers will quite often include separate storage controllers to handle block and file level data.

### Why do we have duplication?

**Data duplication occurs because of the way storage controllers can be configured.**

Storage controllers as their name suggests are the hardware units that control all aspects of storage operation and are therefore are the source of the data we require.

If multiple instances were installed purely for data protection, then they would all operate in an active-standby mode. This is to say one would be active and do all the work; the others would just stay dormant, ready to take over if something happened to the primary.

A single active storage controller would make life easy for us. We would just collect data from that controller and would not have to worry about the others as they would just give us identical answers.

But … array engineers realised a long time ago that that setup is very wasteful of resources. Why not design your arrays so that all the controllers were active all of the time? That way when everything was operating normally (most of the time hopefully) they could share the workload thus giving a massive increase in data throughput and of course they are still available to cope with any failures that came along.

OK, so now if storage controllers can share the workload – why not let them concentrate on slightly different tasks? Let them handle different groups of users with different views of the data.

Sounds like a win-win situation – and it is – for the array manufacturers and owners. The downside for us is that each storage controller might only have a limited view of the storage and to which hosts that storage is connected.

**So we have to collect data from every storage controller!**

Identifying duplication 
------------------------

**Arrays can be identified by a unique serial number.**

An array may be made of many duplicated components but it still exists as a single entity and as such has its own serial number. That number will be stamped on a plate or printed on a label attached to the hardware for humans to read, but it is known internally too.

As we can see the array serial number in the data, we can combine data collections where that number is identical.

**Data collections can only be combined if taken within a small period of time – say 1 hour.**

Combining data collections only makes sense if they were all created with a short period of time, for example 1 hour (the width of the time window should be a configuration item).

Outside of that time window, the profile of the data will most likely have changed thus making any deduplication meaningless.

Where in the processing sequence does de-duplication fit?
---------------------------------------------------------

![Alt text](../Images/processing_sequence.jpg "Processing Sequence")

Note: Virtualisation Linkage is not currently an active process.  It is shown purely to highlight where it would appear in the data processing sequence

**De -duplication should occur after array data has been parsed and loaded and prior to any virtualisation linkage.**

The De-duplication Process
--------------------------

### Considerations

1.  The original data should remain in place so that a diagnostic trail exists from source data all the way to reports.

2.  Maintain/improve data clarity – keep the reporting process as simple as possible.

    The reports should not have to embed all sorts of business logic to exclude/include specific records

### De-duplication Stages

1.  Change current “array” collection to become “storage\_processor”.

    **Potentially a large modification, if reflected throughout the code-base and documentation**

    It would aid clarity greatly if the existing array collection to become storage\_processor. This would more accurately reflect both the source of the data and its contents.

      1.  Add new item “processor_name” – would require additional code to extract from source data.

      2.  All exiting items can remain the same.

    **Alternatives:**

      *  The alternative would be to keep array collection ‘as-is’ and delete/modify existing entries to remove the duplication. This alternative would break ability to track data from source to report.

      *  Introduce a flag to mark the record as deleted. This would require embedding more logic with the reports to exclude records marked as deleted.

2.  Group storage_processor entries on array name and collection date/time.

3.  Create array collection entry.

    1.  Keep existing array structure.

    2.  Entries will be taken from StorageProcessor collection

    3.  Ports will be union of grouped Storage Processor port entries

    4.  Pools will be union of grouped Storage Processor pool entries

    5.  Storage/memory summary values would need to be calculated from merged entries

4.  Create host collection entry.

    Hosts will be a union of grouped Storage Processor host entries

5.  Create disk collection entry.

    Disks will be union of grouped Storage Processor pool entries

6.  Create lun collection entry.

    Luns will be union of grouped Storage Processor lun entries

7.  Points to consider

    1.  Grouping storage_processor entries – do we insist they exist within the same collection? This would remove the need to select within date window.

        Also collection details with array would match those within storage_processor.

    2.  Do we need to generate a new "array_ref_id"?

    3.  Collection details – is it worth maintaining as it is now a merged set?

    4.  Linkages between array, host and disk collections

    5.  Existing entries in host and disk collections. We still have the same considerations to make with them as with array collection (change to processor\_host and processor disk?)


