Concepts - Handling Multiple Source Data Formats In the Processor
=================================================================

Why do we have multiple source data formats?
--------------------------------------------

Storage Fusion Analyze collects configuration data from storage arrays produced by all the leading manufacturers and uses that data to produce a suit of reports to assist storage professionals in managing their storage assets.

Storage Fusion’s data collection tool (the ‘Collector) captures the configuration data from the arrays using the manufacturers’ own command line tools. No two manufacturers’ tools output data in the same format. The Collector simply packages up the data, in its native format, along with some metadata supplied by the user. The data package is then used by the analysis engine (the ‘Processor’) to produce the required reports.

Why not standardise the data format within the collector?
---------------------------------------------------------

One approach would have been to unify the disparate formats at source and submit one known format to the Processor. To do that however would make for a very heavy and very complex collection tool.

To perform the data collection, customers have to download the collector and install it on their local array management consoles. This means we want the Collector to be as light touch and stable as possible.

The current design means all that complexity will be held centrally under direct Storage Fusion control and customer participation is not required in the update process.

Where does variation in data format occur?
------------------------------------------

Data formats will vary with:

-   Vendor

-   Array product line

-   CLI Version

How does the Processor know what sort of data it is dealing with?
-----------------------------------------------------------------

Primary information as to data type is found in the manifest file created by the Collector and packaged with the data.

### Data package structure

The data package, a zip file, produced by the collector contains a manifest file and a sub-folder for each array. The sub-folder takes the arrays name and holds a series of text files. Each file is the output from a specific CLI command.

![Alt text](../Images/Folders_data.jpg "Data Package Structure")

*The contents of an expanded data package - here data has been collected from two arrays: FAS2040-01 and FAS2040-2.*

### Manifest file contents

Manifest files are JSON format text files that describe the contents of the data package. The file consists of three keys: version, time and arrays.

    “version”:  Refers to the version of the Collector that created the file. Over time, this value will allow changes in the data package format and content to be managed by the processor.

    “time”:     Gives the date and time when the collection was made.

    “arrays”:   Details each array present in the package. There will be an entry within arrays for each individual array. The collection will contain sub-folders for each instance of “arrayname” listed.

### Identifying array vendor and which format handler to use

The pertinent keys are within the arrays collection: “vendor” and “family”. The vendor value will equate to the vendor sub-folder in the source tree and the family value identifies the handler.

The example below details two arrays: FAS2040-1 and FAS2040-2. Both were manufactured by NetAPP and will be processed using the FAS7MODE handler

    {
        "version":  "1.0",
        "time":     "2015-07-08T12:00:00.010Z",
        "arrays": [
            {
                "arrayname":    "FAS2040-01",
                "vendor":       "NETAPP",
                "family":       "FAS7MODE",
                "status":       "complete",
                "notes":        "Lorem Ipsum",
                "created_at":  "2015-09-15T10:49:55.415Z"
            },
            {
                "arrayname":    "FAS2040-02",
                "vendor":       "NETAPP",
                "family":       "FAS7MODE",
                "status":       "complete",
                "notes":        "Lorem Ipsum",
                "created_at":  "2015-09-15T10:49:55.415Z"
            }
        ]
    }

Dealing with multiple vendors
-----------------------------

Dealing with multiple vendors is fairly straight-forward. All handler specific code is stored within the processor\\src\\App\\Formats portion of the code tree.

Each vendor has its own sub-folder.

![Alt text](../Images/Folders_vendors.JPG "Vendor folders")

Note: As the name suggests, the ‘Common’ folder holds non-vendor specific code, such as that used for retrieving data from Storage Fusion supplied database tables.

Dealing with multiple product lines
-----------------------------------

Product lines are separated in much the same ways as with the vendor level.

![Alt text](../Images/Folders_handler_detail.jpg "Handler folders")

Each array product line will be processed by a specific handler. Sometimes if their data shares a common format then a handler may process multiple product ranges. An example of this is EMC product ranges Symmetrix and VMAX. The Symmetrix range was replaced by the VMAX range. As the latter is a development of the former they share a common CLI tool, SymCLI, and therefore a common data format.

Note: A handler name will always identify *all* product ranges it supports, e.g. VMAX_SYMMETRIX

Determining the CLI version
---------------------------

The collector is unable to help with CLI version as each CLI will report version information in a different manner and as stated we do not wish to embed that degree of parsing detail into the collector.

Determining what the cli version is always the first task for the handler code.

The structure of the code tree within the handler area reflects this.

![Alt text](../Images/Folders_cli_versions_single.jpg "CLI folders")

There are always three code files present within the handler level folder:

-   ArrayCliVersion.php – this is the routine that extracts the CLI version from the data files.

-   CliMapping.php – contains code that matches CLI version strings to directories holding the required parsing code.

-   FormatProcessList – defines the list of statistic names and the order in which to create them.

Dealing with multiple CLI versions
----------------------------------

Different versions of the same CLI can produce different format output.

These changes can vary from the minor; such as including an extra column in the output or changing the display units of values; to the very complex such as introducing new structural concepts and complex linkages within the data.

***For any given statistic, any variations in its calculation must be handled by a separate routine.** *

This structure ensures that any changes relating to a specific CLI version do not have unforeseen side-effects on other versions. This in turn reduces overhead of testing a new release.

### Storing multiple versions of statistic creation routines

Each statistic creation routine is stored in a separate code file with an identical name, so therefore we need a file structure to keep them apart.

This is achieved by having separate sub-folders for each CLI version. CLI version numbering schemes vary wildly across manufactures. Most use numeric schemes that are easy to order (both for humans and computers), some use more wordy systems, including words like ‘Build’ or Release Candidate’. The latter examples make determining the correct order, programmatically, a nightmare.

To bypass this problem we don’t use the manufacturers’ numbering schemes at all, we simply apply a codename to represent the version. These codenames are simply drawn from a list held in BitBucket.

![Alt text](../Images/Folders_cli_versions_multiple.jpg "CLI folders")

In the example above the VMAX_SYMMETRIX handler has two CLI versions to handle. These two versions have been assigned the codenames ‘Duondu’ and ‘Enism’.

The mapping between CLI version and codename occurs in CliMapping.php.

The salient structure within this code is the mapping array: ‘$map’

e.g.

    $map = [
        'V7.5.0.0' =&gt; 'Duondu',
        'V8.0.0.0' =&gt; 'Enism',
    ];

Here we can easily see how EMC’s CLI version numbers map to the folder names.

The handler now knows where to find the correct statistic creation routines for the data it is processing.

Reducing code duplication – using the default version.
------------------------------------------------------

As described above, the mechanism works perfectly well. We can handle any number of different CLI versions. There is only one drawback – code proliferation.

In the example we have used, the VMAX_SYMMETRIX handler, there are 77 separate statistic generation routines. This means across both CLI versions we would have 154 files. Given that only 5 statistics differ between the versions that is an awful lot of unnecessary code duplication. Code proliferation on this scale would soon become unmanageable!

To prevent this from happen the system uses the concept of a default version. The default version is the only one which has a creation routine for every single statistic. It is the base layer. Subsequent version folders only hold code for those statistics whose creation method differs.

Again the default version is defined in CliMapping.php.

    $default_cli_version = 'V7.5.0.0';
    
    $map = [
        'V7.5.0.0' =&gt; 'Duondu',
        'V8.0.0.0' =&gt; 'Enism',
    ];

**Examples of the incremental approach:**

![Alt text](../Images/Statistic_files.jpg "Statistic Files")

Duondu is the default version and implements creation routines for all 77 statistics.

Enism only holds code for the statistics that require a different creation method.

Internally the handler builds up a list of file paths which take it from the target version back to the default one. The handler searches this list, sequentially, until it locates the desired statistic creation code.

**Example 1:**

Assume we are processing data for CLI Version 'V8.0.0.0'

The handler builds a list of files paths: \['Enism', 'Duondu'\]

Using this path list, the handler will first look for statistic code in the 'Enism'folder. If it does not find it there it will look in the 'Duondu' folder.

So to create the statistic ‘host_luns’ the handler will use the code file ‘Enism\\HostLuns.php’ but to create lun_types it will use ‘Duondu\\LunTypes.php’.

**Example 2:**

Assume we are processing data for CLI Version 'V7.5.0.0'

The handler builds a list of files paths: \['Duondu'\]

The path list only contains the 'Duondu' folder as the desired version is also the default one.

So this time the statistic ‘host_luns’ will be created from the code in ‘Duondu\\HostLuns.php’ and lun_types will be created from the code in ‘Duondu\\LunTypes.php’.

Incremental changes across multiple CLI versions
------------------------------------------------

Let us consider a more complex situation where we are now dealing with four different CLI versions.

Version 'V7.5.0.0' is still the default version and stores a full set of statistics (77) in folder

> Version : 'V7.5.0.0' Folder: 'Duondu' Default version, 77 statistic creation files
> Version : 'V8.0.0.0' Folder: 'Enism' 20 statistic creation files
> Version : 'V8.1.0.0' Folder: 'Sucent' 5 statistic creation files
> Version : 'V8.2.0.0' Folder: 'Atude' 2 statistic creation files

As always the handler created a list of folders in which to search for statistic creation code. The first entry in the list will be the target version (CLI version extracted from the data being processed) and finishing with the default version.

**Example 1:**

Process data for version 'V7.5.0.0' (target version is the default version)

The handler’s internal path list will just be \['Duondu'\]

All creation code will be taken from 'Duondu'

**Example 2:**

Process data for version 'V8.0.0.0' - target version is the default version

The handler’s internal path list will just be \['Enism', 'Duondu'\]

Creation code will be source from the following locations:

-   57 statistics sourced from 'V7.5.0.0' (folder 'Duondu')
-   20 statistics sourced from 'V8.0.0.0' (folder 'Enism')
-   0 statistics sourced from 'V8.1.0.0' (folder 'Sucent')
-   0 statistics sourced from 'V8.2.0.0' (folder 'Atude')

**Example 3:**

Process data for version 'V8.2.0.0' - target version is the default version

The handler’s internal path list will just be \['Atude', 'Sucent', 'Enism', 'Duondu'\]

Creation code will be source from the following locations:

-   50 statistics sourced from 'V7.5.0.0' (folder 'Duondu')
-   20 statistics sourced from 'V8.0.0.0' (folder 'Enism')
-   5 statistics sourced from 'V8.1.0.0' (folder 'Sucent')
-   2 statistics sourced from 'V8.2.0.0' (folder 'Atude')

Coping with both older and newer CLI versions
---------------------------------------------

As show previously the version path system relies principally on the contents of CliMapping.php.

The examples up until now have only really dealt with newer CLI versions being added but what if the customer wants to perform a hardware swap-out exercise and therefore needs to analyse older obsolete arrays?

Well no problem, just add the older CLI version to the list of known versions.

**Note: The code relies on the developer adding the versions in the correct release order.**

    $default_cli_version = 'V7.5.0.0';
        
    $map = [
        'V7.0.0.0' => 'Democee', (new version to handle the old arrays)
        'V7.5.0.0' => 'Duondu',
        'V8.0.0.0' => 'Enism',
        'V8.1.0.0' => 'Sucent',
        'V8.2.0.0' => 'Atude'
    ];

Version 'V7.5.0.0' still remains the default version and again we only need to include statistic creation code to handle the differences between the two versions.

The handler’s mapping routine is sophisticated enough to scan both forwards and backwards from the default version.

When processing 'V7.0.0.0' data the path list will be \['Democee', 'Duondu'\]

Coping with a new CLI versions that does not have any changes
-------------------------------------------------------------

A lot of CLI version upgrades do not result in any changes to the output format of its interrogation commands. Therefore we do not need any new creation code. All we need to do is add a mapping entry for the new version and point it at an existing version folder that will correctly parse its data.

For example:

Version 'V8.2.0.1' is released which just fixes some internal bugs.

There are no changes to ouput formats.

So we add an entry to the map array in CliMapping.php and point it at an existing directory that will correctly handle the data. In this example, 'Atude'

    $default_cli_version = 'V7.5.0.0';
        
    $map = [
        'V7.0.0.0' => 'Democee',
        'V7.5.0.0' => 'Duondu',
        'V8.0.0.0' => 'Enism',
        'V8.1.0.0' => 'Sucent',
        'V8.2.0.0' => 'Atude',
        'V8.2.0.1' => 'Atude'
    ];

What happens when trying to process a CLI version not seen before?
------------------------------------------------------------------

It is a fact of life that customers will encounter new CLI versions before Storage Fusion. Therefore the system should be as resilient as possible in the face of the unknown.

What *should* happen?

1.  The handler fails to locate the position of the target version in the map list.

2.  If target version is alpha-numerically greater than default version then locate the last entry in list less than the target version. Use that as target version.

3.  If target version is alpha-numerically less than default version then locate the first entry in list greater than the target version. Use that as target version.

4.  Create notification that target version not found and what version was used instead.

The handler stands the best chance possible of successfully parsing the data if we can identify a known version closest to the target version. As mentioned previously most CLI version updates do not create changes in their data format. Bug fixes are the most common reason for CLI updates.

Storage fusion should ideally receive notification when the handler is making a ‘guess’ (more accurately an approximation). This way the developers can verify that parsing is indeed being performed correctly.

What *actually* happens?

1.  The handler fails to locate the position of the target version in the map list.

2.  The handler uses the default version as the target version.

Currently the handler does not attempt to find a best match for the target version and simply goes for the default version. Whilst not inherently wrong as time progresses newer CLI versions encountered will be progressively different from the default version and therefore less and less likely to parse successfully.

Also without notification of unhandled CLI versions, system maintenance relies heavily on regular manual processes being performed to ensure parsing is performed correctly.
